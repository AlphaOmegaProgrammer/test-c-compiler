#include <stdio.h>
#include <stdlib.h>
#include <string.h>


char *data_types[] = {
	"void",
	"char",
	"enum",
	"int",
	"long",
	"short",
	"struct"
};

enum data_type{
	VOID,
	CHAR,
	ENUM,
	INT,
	LONG,
	SHORT,
	STRUCT,
	NONE,
	UNKNOWN_TYPE
};

struct symbol{
	enum data_type type;
	char *name;
	char *data;
	struct symbol *next;
} *symbol_root = NULL;

struct command{
	char *command, *symbol;
	struct command *next;
};

struct function{
	enum data_type type;
	char *name;
	struct symbol *argument, *symbol;
	struct command *command;
	struct function *next;
} *func_root = NULL;

enum error_type{
	ERROR,
	WARNING
};



char *this_file_name;
unsigned int line_number = 0;



void error(char *msg, enum error_type error_type){
	static char *error_types[] = {
		"ERROR",
		"warning"
	};

	printf("%s:%hhu %s %s\n", this_file_name, line_number, error_types[error_type], msg);
}

unsigned char white_space(char c){
	return (c == ' ' || c == '\t');
}

void parse_identifier(char **start){
	if((*start[0] > 0x60 && *start[0] < 0x7A) || (*start[0] > 0x40 && *start[0] < 0x5B) || *start[0] == 0x5F){
		(*start)++;
		while((*start[0] > 0x60 && *start[0] < 0x7A) || (*start[0] > 0x40 && *start[0] < 0x5B) || (*start[0] < 0x3A && *start[0] > 0x2F) || *start[0] == 0x5F)
			(*start)++;
	}
}

void parse_literal(char **start){
	if(!strcmp(*start, "NULL")){
		*start += 4;
		return;
	}

	if(*start[0] == '"'){
		char *end = strchr(*start + 1, '"');
		if(end != NULL && end[0] == '"'){
			*start = end+1;
			return;
		}

		error("Invalid string literal", ERROR);
		exit(1);
	}

	char int_type = 'd';
	if((*start)[0] == '0'){
		if((*start)[1] == 'x' || (*start)[1] == 'X'){
			int_type = 'x';
			*start += 2;
		}else if((*start[1]) == 'b'){
			int_type = 'b';
			*start += 2;
		}else if((*start[1]) < 0x30 || (*start[1]) > 0x37){
			int_type = 'o';
			(*start)++;
		}else{
			error("Invalid integer literal", ERROR);
			exit(1);
		}
	}

	char *int_start = *start;
	switch(int_type){
		case 'd':
			for(;*start[0] > 0x2F && *start[0] < 0x3A; (*start)++){}
		break;

		case 'x':
			for(;(*start[0] > 0x2F && *start[0] < 0x3A) || (*start[0] > 0x40 && *start[0] < 0x47) || (*start[0] > 0x60 && *start[0] < 0x67); (*start)++){}
		break;

		case 'b':
			for(;*start[0] == 0x30 && *start[0] == 0x31; (*start)++){}
		break;

		case 'o':
			for(;*start[0] > 0x2F && *start[0] < 0x38; (*start)++){}
		break;
	}

	if(int_start == *start){
		error("Invalid integer literal", ERROR);
		exit(1);
	}
}

struct symbol* find_symbol(char *symbol_name, struct symbol *s_root){
	struct symbol *s = s_root;
	while(s != NULL){
		if(!strcmp(symbol_name, s->name))
			return s;

		s = s->next;
	}

	if(s_root != symbol_root)
		return find_symbol(symbol_name, symbol_root);

	return NULL;
}



int main(int argc, char *argv[]){
	if(argc == 1){
		printf("Usage:\n\n\t%s file.c\n\n", argv[0]);
		return 0;
	}

	if(argc > 2){
		printf("Sorry, can only handle one file at a time right now\n");
		return 0;
	}

	this_file_name = argv[1];
	FILE *f = fopen(argv[1], "r");
	if(f == NULL){
		printf("ERROR: Unable to read file \"%s\"\n", argv[1]);
		return 0;
	}

	char buffer[4096] = {0}, *start = &buffer[0], *end = start + fread(&buffer[0], 1, 4095, f);

	if(end >= start + 4096){
		printf("TOO BIG. FIX THIS YOU LAZY DICK\n");
		exit(1);
	}

	struct function *current_func = NULL;
	enum data_type type = UNKNOWN_TYPE;
	char *identifier = NULL, *old_start;
	while(start < end){
		while(white_space(start[0]))
			start++;

		if(start >= end)
			break;

		if(start[0] == '\n'){
			if(type != UNKNOWN_TYPE){
				if(identifier == NULL)
					error("Data type missing symbol", ERROR);

				error("Missing ;", ERROR);
				exit(1);
			}

			line_number++;
			start++;
			continue;
		}

		if(start[0] == '}'){
			if(current_func != NULL){
				current_func = NULL;
				start++;
				continue;
			}

			error("Extra }", ERROR);
			exit(1);
		}

		// Get data type (if applicable)
		if(type == UNKNOWN_TYPE){
			static size_t type_len;
			for(type = 0; type < NONE; type++){
				type_len = strlen(data_types[type]);
				if(start + 1 + type_len < end && !strncmp(start, data_types[type], type_len) && white_space(start[type_len])){
					start += type_len;
					break;
				}
			}

			if(type != NONE)
				continue;
		}

		if(identifier == NULL){
			old_start = start;
			parse_identifier(&start);

			if(start >= end)
				break;

			if(start == old_start){
				error("Missing symbol name", ERROR);
				exit(1);
			}

			identifier = malloc(start - old_start + 1);
			memcpy(identifier, old_start, start - old_start);
			identifier[start - old_start] = '\0';

			continue;
		}

		switch(start[0]){
			case '=':
			case ';':
				if(type == UNKNOWN_TYPE || type == NONE){
					error("Invalid symbol data type", ERROR);
					exit(1);
				}

				if(identifier == NULL){
					error("Missing symbol name", ERROR);
					exit(1);
				}

				struct symbol *s = malloc(sizeof(struct symbol));
				s->type = type;
				s->name = identifier;

				if(start[0] == '='){
					start++;
					while(white_space(start[0]))
						start++;

					if(start >= end)
						break;

					old_start = start;
					parse_identifier(&start);

					if(old_start == start){
						parse_literal(&start);

						if(old_start == start){
							error("Unrecognized literal", ERROR);
							exit(1);
						}

						s->data = malloc(start - old_start + 1);
						memcpy(s->data, old_start, start - old_start);
						s->data[start - old_start] = '\0';
					}else{
						s->data = malloc(start - old_start + 1);
						memcpy(s->data, old_start, start - old_start);
						s->data[start - old_start] = '\0';

						if(memcmp(old_start, "NULL", 4) != 0 || start - old_start != 4){
							struct symbol *found_symbol;
							if(current_func == NULL)
								found_symbol = find_symbol(s->data, symbol_root);
							else
								found_symbol = find_symbol(s->data, current_func->symbol);

							if(found_symbol == NULL){
								error("Unrecognized symbol", ERROR);
								exit(1);
							}
						}
					}

					if(start[0] != ';'){
						error("Uh... Missing a semi colon?", ERROR);
						exit(1);
					}
				}else
					s->data = NULL;

				if(current_func == NULL){
					s->next = symbol_root;
					symbol_root = s;
				}else{
					s->next = current_func->symbol;
					current_func->symbol = s;
				}

				start++;
				type = UNKNOWN_TYPE;
				identifier = NULL;
			break;

			case '(':
				// Just ignore function arguments for now becuase I'm lazy
				start = strchr(start, ')');
				if(start == NULL){
					error("Missing )", ERROR);
					exit(1);
				}

				start++;
				while(white_space(start[0]) || start[0] == '\n'){
					if(start[0] == '\n')
						line_number++;

					start++;
				}

				current_func = malloc(sizeof(struct function));
				current_func->type = type;
				current_func->name = identifier;
				current_func->argument = NULL;
				current_func->command = NULL;
				current_func->symbol = NULL;

				current_func->next = func_root;
				func_root = current_func;

				if(start[0] == '{')
					start++;
				else if(start[0] == ';'){
					current_func = NULL;
					start++;
				}else{
					error("Invalid syntax", ERROR);
					exit(1);
				}

				type = UNKNOWN_TYPE;
				identifier = NULL;
			break;

			default:
				if(current_func == NULL){
					error("Line should be inside of a function", ERROR);
					exit(1);
				}

				struct command *c;
				if(current_func->command == NULL){
					current_func->command = malloc(sizeof(struct command));
					c = current_func->command;
				}else{
					c = current_func->command;
					while(c->next != NULL)
						c = c->next;

					c->next = malloc(sizeof(struct command));
					c = c->next;
				}

				c->command = identifier;
				old_start = start;
				parse_identifier(&start);

				if(start[0] != ';'){
					error("Missing ;", ERROR);
					exit(1);
				}

				c->symbol = malloc(start - old_start + 1);
				memcpy(c->symbol, old_start, start - old_start);
				c->symbol[start - old_start] = '\0';

				c->next = NULL;

				start++;
				type = UNKNOWN_TYPE;
				identifier = NULL;
			break;
		}
	}

	if(type != UNKNOWN_TYPE || identifier != NULL){
		error("Unexpected end of file", ERROR);
		exit(1);
	}

	size_t len = strlen(argv[1]);
	char *out_file_name = malloc(len + 3);
	memcpy(out_file_name, argv[1], len);
	out_file_name[len-1] = 'a';
	out_file_name[len] = 's';
	out_file_name[len+1] = 'm';
	out_file_name[len+2] = '\0';

	FILE *out_file = fopen(out_file_name, "w");
	if(out_file == NULL){
		printf("Failed to open file \"%s\" for output...?\n", out_file_name);
		exit(1);
	}

	struct function *func = func_root;
	while(func != NULL){
		fprintf(out_file, "%s:\n", func->name);

		if(func->argument != NULL){
			// DO SOME STUFF HERE
		}

		if(func->symbol != NULL){
			// DO SOME MORE STUFF HERE
		}

		struct command *command = func->command;
		while(command != NULL){
			char *value;
			struct symbol *s = find_symbol(command->symbol, func->symbol);

			if(s == NULL)
				value = command->symbol;
			else
				value = s->data;

			if(!strcmp(command->command, "return"))
				fprintf(out_file, "\tMOV al, %s\n\tret\n\n", value);

			command = command->next;
		}

		func = func->next;
	}

	fclose(f);
	fclose(out_file);

	return 0;
}
